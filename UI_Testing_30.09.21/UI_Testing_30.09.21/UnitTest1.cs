using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace UI_Testing_30._09._21
{
    public class Tests
    {
        Registration registration = new Registration();
       

        [SetUp]
        public void Setup()
        {
            registration.LaunchBrowser();
        }


        [Theory]
        [TestCase("", "!!!!!!!", "!@bb.com", "123!", "123!", "0123654644", "Required")]
        //[TestCase("6", "", "2@2v.com", "dadada1!", "Qwerty1!", "1111112222", "Required")]
        //[TestCase("a111111111111aaa1111111111111111111111111111111111111111111111111aaa", "bb1111111111111111111111111111111111111111111bbb1111111111111111111111111111111111111111111111111bbb", "vvvvv2222222222222222vvv222222222222vvvvvv2222222222222222222222222222222222222222222222222@gmail.com", "Qwerty123?125nn22iAAAAn0!", "Qwerty123?125nn22iAAAAn0!", "9999999999", "Required")]


        public void Test1(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string experror)
        {
            registration.Input_RegistrationFieldsPage1(firstname, lastname, email, password, confirmpassword, mobNumber);
            var acterror = registration.driver.FindElement(By.CssSelector("input[name='first_name']+div[class^='FormErrorText'] > div")).Text;
                Assert.AreEqual(experror, acterror, "mistake. It's wrong");
        }






        //[Theory]
        //[TestCase("Dadadada", "DadaDada", "jgfjfdk87979@nbnb.com", "Dadadada123!", "Dadadada123!","8597521236", "https://newbookmodels.com/join")]
        //[TestCase("K", "2", "4@kkkk.com", "Dada1!", "Dada1!", "5279879632","https://newbookmodels.com/join")]
        //[TestCase("a111111111111aaa1111111111111111111111111111111111111111111111111aaa", "bb1111111111111111111111111111111111111111111bbb1111111111111111111111111111111111111111111111111bbb", "vvvvv2222222222222222vvv222222222222vvvvvv2222222222222222222222222222222222222222222222222@gmail.com", "Qwerty123?125nn22iAAAAn0!", "Qwerty123?125nn22iAAAAn0!", "9999999999", "https://newbookmodels.com/join")]


        //public void Test1 (string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string expextedURL)
        //{
        //    registration.Input_RegistrationFieldsPage1(firstname, lastname, email, password, confirmpassword, mobNumber);
        //    Assert.AreEqual(expextedURL,registration.driver.Url, "mistake. It's wrong");
        //}

        //[Theory]
        //[TestCase("��������", "��������", "���02020236@����.com", "Qwerty123!", "Qwerty123!", "1234560789", "https://newbookmodels.com/join")]
        //[TestCase("�", "2", "2@b�b.com", "Qwerty1!", "Qwerty1!", "1111112222", "https://newbookmodels.com/join")]
        //[TestCase("�111111111111���1111111111111111111111111111111111111111111111111���", "��1111111111111111111111111111111111111111111���1111111111111111111111111111111111111111111111111���", "vvvvv2222222222222222vvv222222222222vvvvvv2222222222222222222222222222222222222222222222222@gmail.com", "Qwerty123?125nn22iAAAAn0!", "Qwerty123?125nn22iAAAAn0!", "9999999999", "https://newbookmodels.com/join")]


        //public void Test2(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string expextedURL)
        //{
        //    registration.Input_RegistrationFieldsPage1(firstname, lastname, email, password, confirmpassword, mobNumber);
        //    Assert.AreEqual(expextedURL, registration.driver.Url, "mistake. It's wrong");
        //}

        //[Theory]
        //[TestCase("Lunalina", "Linaluna", "gew6667228966@btkylj.com", "Qwerty123!", "Qwerty123!", "1234560789", "dadadadddd", "gaga.com", "USA", "https://newbookmodels.com/join/company")]
        //[TestCase("Lunalina", "Linaluna", "gew66672288889@btkylj.com", "Qwerty123!", "Qwerty123!", "1234560789", "3","M.com", "Usa", "https://newbookmodels.com/join/company")]
        //[TestCase("Lunalina", "Linaluna", "gew66672222289@btkylj.com", "Qwerty123!", "Qwerty123!", "1234560789", "dadada3467dadadadaaaaaaaaaaaaaaaaaaaaaa9999aaaaaaaaaaaa", "kkkkkkkkkkkk777777777hjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh.com", "USA", "https://newbookmodels.com/join/company")]


        //public void Test2(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber, string companyname, string compwebsite, string adressstring, string expextedURL)
        //{
        //    registration.Input_RegistrationFieldsPage1(firstname,  lastname,  email,  password,  confirmpassword,  mobNumber);
        //    registration.Input_RegistrationFieldsPage2(companyname, compwebsite, adressstring);
        //    Assert.AreEqual(expextedURL, registration.driver.Url, "mistake. It's wrong");

        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    Thread.Sleep(4000);
        //    registration.ClosedBrowser();
        //}
    }
}
