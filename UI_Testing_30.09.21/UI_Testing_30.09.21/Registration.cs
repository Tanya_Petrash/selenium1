﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace UI_Testing_30._09._21

{
   public class Registration
    {
       public IWebDriver driver;
        public void LaunchBrowser()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://newbookmodels.com/");
            driver.Manage().Window.Maximize(); //открытие браузера на всю страницу
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10); // вместо  Thread Sleep
        }
        public void Input_RegistrationFieldsPage1(string firstname, string lastname, string email, string password, string confirmpassword, string mobNumber)
        {
            //IWebElement вместо переменной
              IWebElement signUp_Button = driver.FindElement(By.XPath("//button[@class='Navbar__signUp--12ZDV']"));
            signUp_Button.Click();
            IWebElement firstNameField = driver.FindElement(By.Name("first_name"));
            firstNameField.SendKeys(firstname);
            IWebElement lastNameField = driver.FindElement(By.Name("last_name"));
            lastNameField.SendKeys(lastname);
            IWebElement emailField = driver.FindElement(By.Name("email"));
            emailField.SendKeys(email);
            IWebElement passwordField = driver.FindElement(By.Name("password"));
            passwordField.SendKeys(password);
            IWebElement confirmPasswordFieldField = driver.FindElement(By.Name("password_confirm"));
            confirmPasswordFieldField.SendKeys(confirmpassword);
            IWebElement mobNumberFieldField = driver.FindElement(By.Name("phone_number"));
            mobNumberFieldField.SendKeys(mobNumber);
            IWebElement submit_Button = driver.FindElement(By.XPath("//button[@type='submit']"));
            submit_Button.Click();

        }
        public void Input_RegistrationFieldsPage2(string companyname, string compwebsite, string adress)
        {

            
            IWebElement companyNameField = driver.FindElement(By.Name("company_name"));
            companyNameField.SendKeys(companyname);
            IWebElement compwebsiteField = driver.FindElement(By.Name("company_website"));
            compwebsiteField.SendKeys(compwebsite);
            //Thread.Sleep(500);
            //driver.FindElement(By.Name("location")).Click();
            //driver.FindElement(By.Name("location")).SendKeys(Keys.ArrowDown);
            IWebElement adressField = driver.FindElement(By.Name("location"));
            IWebElement location = driver.FindElement(By.Name("_hjRemoteVarsFrame")); //frame это типо сайт в сайте,т.к. выпадающее гугловское окно
            adressField.SendKeys(adress);
            adressField.SendKeys(Keys.Up); //кликнуть стрелкой вверх
            Actions action = new Actions(driver);  
            action.MoveToElement(location);
            action.KeyDown(Keys.LeftShift);
            IWebElement industry_Button = driver.FindElement(By.XPath("//input[@class='Select__valueBox--2VlHF']"));
            industry_Button.Click();
            IWebElement select1 = driver.FindElement(By.XPath("/html/body/nb-app/nb-signup-company/common-react-bridge/div/div[2]/div/section/section/div/form/section/section/div/div[2]/section/div/section/div[3]/div[1]/div/div[1]/div[2]/div[4]/span"));
            select1.Click();
            IWebElement finish_Button = driver.FindElement(By.XPath("//button[@type='submit']"));
            finish_Button.Click();

        }
        //        List<string> yearsDropdown = new List<string>() { "Select...", "Before 1996", "1996", "1997", "1998", "1999", "2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016" };
        //        IList<IWebElement> dropdownList = wd.FindElements(By.CssSelector("#Components_0__Entity_SourceInfo_Value_Entity_ModelYear"));
        //foreach(IWebElement i in dropdownList)
        //{
        //    Assert.AreEqual(i.Text, yearsDropdown.Text);
        //}
        public void ClosedBrowser()
        {
            driver.Quit();
        }








    }
}
